import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [
      createPersistedState(),
  ],
  state: {
    api: 'apiplatform',
    token: '',
    username: '',
    role: '',
  },
  mutations: {
    changeApi(state, newApi) {
      if (newApi) {
        state.api = newApi
      }      
    },
    changeToken(state, newToken) {
      state.token = newToken
    },
    changeUsername(state, newUsername) {
      state.username = newUsername
    },
    changeRole(state, newRole) {
      state.role = newRole
    }
  },
  getters: {
    getApi: state => {
      return state.api
    },
    getToken: state => {
      return state.token
    },
    getUsername: state => {
      return state.username
    },
    getRole: state => {
      return state.role
    }
  },
  modules: {
  }
})
