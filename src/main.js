import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false


//FONT AWESOME
import {library} from '@fortawesome/fontawesome-svg-core'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import fontAwesomeFree from '@fortawesome/fontawesome-free'

library.add(fas, far, fab, fontAwesomeFree)
Vue.component('font-awesome-icon', FontAwesomeIcon)



new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
